<%-- 
    Document   : drivers
    Created on : 7 мар. 2023 г., 12:56:21
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

      <!-- Broadcums -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/BusStationControlRoom/">Главная</a></li>
          <li class="breadcrumb-item"><a href="#">Водители</a></li>
          <!--li class="breadcrumb-item active" aria-current="page">Данные</li-->
        </ol>
      </nav>
      <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Список водителей</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                  <div class="btn-group mr-2">
                    <a type="button" class="btn btn-sm btn-outline-secondary" href="/BusStationControlRoom/add_driver.jsp">Добавить</a>
                    <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                  </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Фамилия</th>
                        <th>Имя</th>
                        <th>Отчество</th>
                        <th>Г.р.</th>
                        <th>Статус</th>
                        <th>Действие</th>
                      </tr>
                    </thead>
                    <tbody>
                        <% for(int i=1; i < 10; i++) { %>
                            <tr>
                              <td><%=i %></td>
                              <td>Петров</td>
                              <td>Иван</td>
                              <td>Сидорович</td>
                              <td>20.10.1999</td>
                              <td>Работате</td>
                              <td>
                                  <a href="#" class="ml-1">
                                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                      </svg>
                                  </a>
                              </td>
                            </tr>
                        <% } %>
                      
                    </tbody>
                </table>
            </div>
        </main>
    </div>
