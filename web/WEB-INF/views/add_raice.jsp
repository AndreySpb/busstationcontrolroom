<%-- 
    Document   : add_raice
    Created on : 7 мар. 2023 г., 17:34:19
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <!-- Broadcums -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb active">
        <li class="breadcrumb-item"><a href="/BusStationControlRoom/">Главная</a></li>
        <li class="breadcrumb-item"><a href="/BusStationControlRoom/routers.jsp">Рейсы</a></li>
        <li class="breadcrumb-item active" aria-current="page">Новый рейс</li>
      </ol>
    </nav>
    <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Новый рейс</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputRout">Маршрут</label>
                            <select id="inputRout" class="form-control" name="rout">
                                <option selected>Выбрать...</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputDtRaice">Дпта отправления</label>
                            <input type="text" class="form-control" id="inputDtRaice" name="dtRaice">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputTimeRaice">Время отправления</label>
                            <input type="text" class="form-control" id="inputTimeRaice" name="timeRaice">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputBus">Автобус</label>
                             <select id="inputBus" class="form-control" name="bus">
                                <option selected>Выбрать...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputDriver">Водитель</label>
                            <select id="inputDriver" class="form-control" name="driver">
                                <option selected>Выбрать...</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="/BusStationControlRoom/routers.jsp" class="btn btn-secondary">Назад</a>
                </form>
            </div>
        </main>
    </div>