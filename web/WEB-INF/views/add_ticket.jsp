<%-- 
    Document   : add_ticket
    Created on : 7 мар. 2023 г., 14:38:37
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
      <!-- Broadcums -->
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Главная</a></li>
          <li class="breadcrumb-item"><a href="/BusStationControlRoom/tickets.jsp">Билеты</a></li>
          <li class="breadcrumb-item active" aria-current="page">Продажа</li>
        </ol>
      </nav>
      <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Продажа билета на рейс №1203</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputFio">Маршрут</label>
                            <select id="inputStatus" class="form-control" name="status">
                                <option selected>Выбрать...</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputdtRout">Дата отправления</label>
                            <input type="date" class="form-control" id="inputdtRout" name="dtRout">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputSurname">Cумма</label>
                        <input type="text" class="form-control" id="inputSurname" name="surname">
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="/BusStationControlRoom/tickets.jsp" class="btn btn-secondary">Назад</a>
                </form>
            </div>
        </main>
    </div>