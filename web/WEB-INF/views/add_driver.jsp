<%-- 
    Document   : add_driver
    Created on : 7 мар. 2023 г., 13:33:38
    Author     : andrey
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

    <!-- Broadcums -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb active">
        <li class="breadcrumb-item"><a href="/BusStationControlRoom/">Главная</a></li>
        <li class="breadcrumb-item"><a href="/BusStationControlRoom/routers.jsp">Водители</a></li>
        <li class="breadcrumb-item active" aria-current="page">Новый водитель</li>
      </ol>
    </nav>
    <!-- ./EndBroadcums -->
    
    <div class="row">

        <main role="main" class="col-md-9 m-auto col-lg-10 px-md-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Добавить водителя</h1>
            </div>
            <div class="col-md-8 col-sm-12">
                <form>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputFio">Фамилия</label>
                        <input type="text" class="form-control" id="inputFio" name="fam">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputName">Имя</label>
                        <input type="text" class="form-control" id="inputName" name="name">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputSurname">Отчество</label>
                      <input type="text" class="form-control" id="inputSurname" name="surname">
                    </div>
                    <div class="form-group">
                      <label for="inputBirthday">Год рождения</label>
                      <input type="data" class="form-control" id="inputBirthday" name="birthday">
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="inputStatus">Статус</label>
                        <select id="inputStatus" class="form-control" name="status">
                          <option selected>Выбрать...</option>
                          <option value="1">Работате</option>
                          <option value="2">Простаивает</option>
                          <option value="3">Отпуск</option>
                          <option value="4">Больничный</option>
                          <option value="5">Уволен</option>

                        </select>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                    <a href="/BusStationControlRoom/drivers.jsp" class="btn btn-secondary">Назад</a>
                </form>
            </div>
        </main>
    </div>